package com.gillian.observermode;

// concrete observer -- Sina
public class Sina implements Observer {
    private float temperature;
    private float pressure;
    private float humidity;

    public void update(float temperature, float pressure, float humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        display();
    }

    public void display() {
        System.out.println("Sina");
        System.out.println("Current temperature " + temperature);
        System.out.println("Current pressure " + pressure);
        System.out.println("Current humidity " + humidity);
        System.out.println("");
    }

}
