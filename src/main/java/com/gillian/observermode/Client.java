package com.gillian.observermode;

public class Client {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        Baidu baiduWebsite = new Baidu();
        Sina sinaWebsite = new Sina();

        weatherData.addObserver(baiduWebsite);
        weatherData.addObserver(sinaWebsite);

        weatherData.setData(20, 80, 100);
    }

}
