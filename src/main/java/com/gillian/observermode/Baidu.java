package com.gillian.observermode;

// concrete observer -- Baidu
public class Baidu implements Observer{
    private float temperature;
    private float pressure;
    private float humidity;

    public void update(float temperature, float pressure, float humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        display();
    }

    public void display() {
        System.out.println("Baidu");
        System.out.println("Current temperature " + temperature);
        System.out.println("Current pressure " + pressure);
        System.out.println("Current humidity " + humidity);
        System.out.println("");
    }

}
